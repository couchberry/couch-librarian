===============
couch-librarian
===============

.. image:: https://readthedocs.org/projects/couch-librarian/badge/?version=latest
   :target: http://couch-librarian.readthedocs.org/en/latest/?badge=latest
   :alt: Documentation Status

Application for converting CouchDB documents from and to filesystem representation using CouchDB Python library.

Latest documentation can be found on `RTFD.com <http://couch-librarian.readthedocs.org/en/latest>`_.

Filesystem Layout
=================
.. note:: The filesystem layout used by couch-librarian is the same as used by `CouchApp
          <http://couchapp.readthedocs.org/en/latest/design/filesystem-mapping.html>`_.
