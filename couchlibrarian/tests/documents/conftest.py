# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, Tomas Hozza (couchberries.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of couch-librarian nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import tarfile

import pytest

from couchlibrarian.documents import DESIGN_DOC_PREFIX_STR
from couchlibrarian.documents import DOCUMENT_ID_STR
from couchlibrarian.documents import ATTACHMENTS_STR


test_app_archive = os.path.join(os.path.dirname(__file__), 'test-app.tar.gz')


@pytest.fixture
def docdir(tmpdir):
    """
    Fixture which creates sanely named document directory inside temporary directory and returns it.
    """
    doc_name = 'test-app'
    return tmpdir.mkdir(doc_name)


@pytest.fixture
def docdir_with_idfile(docdir):
    """
    Fixture which creates _id file inside document directory and returns the directory.
    """
    id_file = docdir.join(DOCUMENT_ID_STR)
    id_file.write(docdir.basename)
    return docdir


@pytest.fixture
def ddocdir_with_idfile(docdir):
    """
    Fixture which creates _id file inside document directory and returns the directory. The document is a design doc.
    """
    id_file = docdir.join(DOCUMENT_ID_STR)
    id_file.write('{}/{}'.format(DESIGN_DOC_PREFIX_STR, docdir.basename))
    return docdir


@pytest.fixture
def test_app_dir(tmpdir):
    """
    Extract test-app generated with CouchApp application into temporary directory.

    Creates structure:
    tmpdir/
        test-app/
            _attachments/
                index.html
                script/
                    app.js
                style/
                    main.css
            lists/
            shows/
            updates/
            vendor/
                couchapp/
                    _attachments/
                        jquery.couch.app.js
                        jquery.couch.app.util.js
                        jquery.couchForm.js
                        jquery.couchLogin.js
                        jquery.couchProfile.js
                        jquery.mustache.js
                        md5.js
                    metadata.json
            views/
                recent-items/
                    map.js
            .couchappignore
            .couchapprc
            README.md
            _id
            couchapp.json
            language
    """
    with tarfile.open(test_app_archive) as archive:
        archive.extractall(str(tmpdir))

    appdir = tmpdir.join('test-app')
    return appdir


@pytest.fixture
def app_js_file(test_app_dir):
    f = test_app_dir.join('_attachments', 'script', 'app.js')
    return f
