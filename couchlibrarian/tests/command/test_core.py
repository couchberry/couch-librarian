# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, Tomas Hozza (couchberries.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of couch-librarian nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import random
import string

import pytest

from couchlibrarian.command.core import CommandBase


class TestCommandBase(object):
    """
    Test CommandBase class
    """

    def test_new_class_with_command(self):
        """
        Test that creation of class with defined _command attribute works fine.
        """
        class NewCommand(CommandBase):
            _command = 'command_{}'.format(''.join([random.choice(string.ascii_letters) for _ in range(5)]))
            pass

        assert NewCommand

    def test_new_class_without_command(self):
        """
        Test that creation of class without defined _command attribute fails.
        """
        with pytest.raises(TypeError):
            class NewCommand(CommandBase):
                pass

    def test_new_class_with_existing_command_name(self):
        """
        Test that creation of class with defined _command attribute which is already defined fails.
        """
        command_name = 'command_{}'.format(''.join([random.choice(string.ascii_letters) for _ in range(5)]))

        class NewCommand(CommandBase):
            _command = command_name
            pass

        with pytest.raises(ValueError):
            class AnotherNewCommand(CommandBase):
                _command = command_name
                pass

    def test_instantation_without_defined_abs_methods_fails(self):
        """
        Test that instantiation of CommandBase class subclass without implemented all abstract methods fails.
        """
        class NewCommand(CommandBase):
            _command = 'command_{}'.format(''.join([random.choice(string.ascii_letters) for _ in range(5)]))
            pass

        with pytest.raises(TypeError):
            assert NewCommand()

    def test_instantation_with_defined_abs_methods_works(self):
        """
        Test that instantiation of CommandBase class subclass with implemented all abstract methods works.
        """
        class NewCommand(CommandBase):
            _command = 'command_{}'.format(''.join([random.choice(string.ascii_letters) for _ in range(5)]))

            @classmethod
            def configure_parser(cls, subparsers):
                pass

            @staticmethod
            def run(*args, **kwargs):
                pass

        assert NewCommand()

    def test_calling_abstractmethods_on_base_class_fails(self):
        """
        Test that calling abstract methods on base class fails.
        """

        with pytest.raises(NotImplementedError):
            CommandBase.configure_parser(None)

        with pytest.raises(NotImplementedError):
            CommandBase.run()
