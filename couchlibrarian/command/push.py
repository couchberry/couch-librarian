# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, Tomas Hozza (couchberries.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of couch-librarian nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os

import couchdb

from .core import CommandBase
from ..logger import LOGGER
from ..documents import doc_from_path


class PushCommand(CommandBase):
    """
    Class implementing 'push' command
    """

    _command = 'push'

    @classmethod
    def configure_parser(cls, subparsers):
        parser = subparsers.add_parser(cls._command)
        parser.add_argument('path', help='Path in which to look for database directories containing ddoc')
        parser.add_argument('--dry-run', action='store_true', default=False)
        parser.set_defaults(func=cls.run)

    @staticmethod
    def run(options, *args, **kwargs):
        PushCommand.push(options.path, options.db_url, options.dry_run, options.database)

    @staticmethod
    def push(path, db_url, dry_run=False, database=None):
        dbs = dict()
        # if database name is specified, treat the path as directory containing design document
        if database:
            dbs[database] = [doc_from_path(path)]
        else:
            dbs.update(PushCommand.process_directory_with_databases(path))
        if not dry_run:
            PushCommand.push_ddocs_to_databases(dbs, db_url)
        else:
            print(dbs)
        return dbs

    @staticmethod
    def _get_dirs_from_path(path):
        """
        Returns list with paths to all directories contained inside the given path

        :param path: Patch where to look for directories
        :return: list of directories in the given path
        """
        # TODO: the listing of directories could be possibly done in a more efficient way
        return [os.path.join(path, x) for x in os.listdir(path) if os.path.isdir(os.path.join(path, x))]

    @staticmethod
    def process_database_dir(path):
        """
        Processes a single directory representing a database.

        Each directory inside the database directory is treated as if it represents design document

        :param path: Path to the directory representing a database
        :return:
        """
        LOGGER.debug("Processing database directory '%s'", path)

        ddocs = list()
        # process all directories representing design documents
        dirs = PushCommand._get_dirs_from_path(path)
        for ddoc_dir in dirs:
            LOGGER.debug("Processing ddoc '%s'", ddoc_dir)
            ddocs.append(doc_from_path(ddoc_dir))

        return ddocs

    @staticmethod
    def process_directory_with_databases(path):
        """
        Processes all directories in the given path and treats them as a database name

        :param path: Path in which to look for directories representing database
        :return: None
        """
        LOGGER.debug("Processing database directories in '%s'", path)

        dbs = dict()
        dirs = PushCommand._get_dirs_from_path(path)
        for db_dir in dirs:
            dbs[os.path.basename(db_dir)] = PushCommand.process_database_dir(db_dir)

        return dbs

    @staticmethod
    def push_ddocs_to_databases(dbs_with_ddocs, server_url=None):
        """
        Goes through the dictionary with database names and lists containing design documents to push. Pushes the changes
        to the CouchDB server.

        :param dbs_with_ddocs: dict containing {'db_name': [ddoc1, ddoc2, ...], ...}
        :return: None
        """
        if server_url is not None:
            server = couchdb.Server(url=server_url)
        else:
            server = couchdb.Server()

        for db_name, ddocs in dbs_with_ddocs.items():
            if db_name not in server:
                server.create(db_name)
            db = server[db_name]
            for ddoc in ddocs:
                db.save(ddoc)
