# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, Tomas Hozza (couchberries.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of couch-librarian nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from abc import ABCMeta
# needed to make metaclass programming work in Py2 and Py3
from six import with_metaclass

from ..py23 import abstractclassmethod
from ..py23 import abstractstaticmethod


COMMANDS_REGISTRY = dict()


class MetaCommandBase(ABCMeta):
    """
    Meta class registering new commands. This metaclass should not be used directly. You should subclass the
    CommandBase class.
    """

    def __new__(mcs, name, bases, namespace):
        """
        Method creating the class and registering it into COMMANDS_REGISTRY under key stored in 'COMMAND'.
        """
        cls = super(MetaCommandBase, mcs).__new__(mcs, name, bases, namespace)

        if name != 'CommandBase':
            # register only subclasses of CommandBase class
            try:
                command = namespace['_command']
            except KeyError:
                raise TypeError("CommandBase subclass has to define '_command' attribute")

            if COMMANDS_REGISTRY.get(command, None) is not None:
                raise ValueError("Command '{0}' is already already defined".format(name))

            COMMANDS_REGISTRY[command] = cls

        return cls


class CommandBase(with_metaclass(MetaCommandBase)):
    """
    Base class for any command implementation.
    """

    _command = None

    @abstractclassmethod
    def configure_parser(cls, subparsers):
        raise NotImplementedError()

    @abstractstaticmethod
    def run(*args, **kwargs):
        raise NotImplementedError()
