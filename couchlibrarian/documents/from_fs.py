# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, Tomas Hozza (couchberries.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of couch-librarian nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""Module implementing conversion of the filesystem representation of a CouchDB document to a dictionary
representing the document, which can be directly pushed to CouchDB."""

import os
import json
import base64
import mimetypes

from . import ATTACHMENTS_STR
from . import DESIGN_DOC_PREFIX_STR
from . import DOCUMENT_ID_STR

from ..logger import LOGGER
from ..py23 import file_not_found_error


__all__ = ['doc_from_path']


def get_docid_from_path(doc_root, design=None):
    """Return ID of the document, that is being converted from filesystem representation.

    :param doc_root: Path to the root directory of the filesystem representation of a CouchDB document.
    :type doc_root: str
    :param design: Whether the document is a design document. This option is ignored in case the directory contains
                   '_id' file with the document ID.
    :type design: bool
    :return: ID of the document. If is is a design document, the name is prefixed with '_design'.
    :rtype: str
    """
    # try to use the _id file if it exists
    try:
        with open(os.path.join(doc_root, DOCUMENT_ID_STR)) as f:
            docid = f.readline().rstrip('\n')
    except file_not_found_error:
        docid = None

    # if we don't have docid yet, use the name of the directory
    if not docid:
        docid = os.path.basename(doc_root)
    else:
        design = True if docid.startswith(DESIGN_DOC_PREFIX_STR) else False

    # normalize the docid and return it
    id_base = DESIGN_DOC_PREFIX_STR + '/{name}' if design else '{name}'
    return id_base.format(name=docid.split('/')[-1])


def get_attachment_from_file(path):
    """Returns dict with all the necessary information for an attachment

    :param path: Path to file from which to generate attachment representation used by CouchDB.
    :type path: str
    :return: Dictionary containing the mime type of the file attachment and Base64 encoded content of the file.
    :rtype: dict
    """
    attributes = dict()
    with open(path, 'rb') as f:
        attributes['content_type'] = ';'.join(filter(None, mimetypes.guess_type(path)))
        attributes['data'] = base64.b64encode(f.read()).decode()
    return attributes


def get_attachments_from_path(doc_root, path):
    """Construct a dictionary with all attachments contained in the '_attachments' directory under the given path. The
    passed 'path' must be a subdirectory of doc_root.

    :param doc_root: Path to the root directory of the filesystem representation of a CouchDB document.
    :type doc_root: str
    :param path: Path to directory which contains some attachments.
    :type path: str
    :return: Dictionary with file names of all attachments found under the given path and their type and content.
    :rtype: dict
    """
    attachments = dict()
    attachments_dir = os.path.join(path, ATTACHMENTS_STR)

    if not os.path.isdir(attachments_dir):
        return attachments

    # walk through the directory and add files to the dict
    for dirpath, dirnames, filenames, in os.walk(attachments_dir):
        # determine the prefix path of the attachment file
        att_name_prefix = os.path.join(
            '',
            *[x for x in os.path.relpath(dirpath, doc_root).split(os.path.sep) if x != ATTACHMENTS_STR]
        )
        # process every file in the current directory
        for name in filenames:
            attachments[os.path.join(att_name_prefix, name)] = get_attachment_from_file(
                os.path.join(dirpath, name)
            )
    return attachments


def doc_from_path(doc_root):
    """Converts the filesystem representation of a CouchDB document under the given path to a dictionary which
    can be directly pushed to CouchDB.

    :param doc_root: Path to the root directory of the filesystem representation of a CouchDB document.
    :type doc_root: str
    :return: Dictionary representing the converted document.
    :rtype: dict
    """
    document = dict()
    if not os.path.exists(doc_root):
        raise ValueError("Path {} does not exist".format(doc_root))

    # Create the _id of the document
    document[DOCUMENT_ID_STR] = get_docid_from_path(doc_root)

    # go through files
    for dirpath, dirnames, filenames, in os.walk(doc_root):
        # if there are any attachments in the current directory, process them
        if ATTACHMENTS_STR in dirnames:
            try:
                # add new attachments to the list of existing attachments
                attachments = document[ATTACHMENTS_STR]
            except KeyError:
                # if this is the fist time we are adding attachments, then create the dict
                attachments = document[ATTACHMENTS_STR] = dict()
            attachments.update(get_attachments_from_path(doc_root, dirpath))
            # remove the _attachments dir from list so that is is not processed
            dirnames.remove(ATTACHMENTS_STR)

        # filter out any hidden files - starting with "." or "_"
        filenames = [x for x in filenames if not x.startswith('.') and not x.startswith('_')]
        # if there are some regular files in the directory, create the necessary structure in the document
        if filenames:
            dict_to_add_files_to = document
            dirs_to_file_from_root = [x for x in dirpath.lstrip(doc_root).split(os.path.sep) if x]
            # create the necessary structure
            for d in dirs_to_file_from_root:
                try:
                    dict_to_add_files_to = dict_to_add_files_to[d]
                except KeyError:
                    dict_to_add_files_to[d] = dict()
                    dict_to_add_files_to = dict_to_add_files_to[d]

            # add all files to the document
            for filename in filenames:
                name, extension = os.path.splitext(filename)
                with open(os.path.join(dirpath, filename)) as f:
                    # handle JSON file
                    if extension == '.json':
                        dict_to_add_files_to[name] = json.load(f)
                    # for all other files just read the content
                    else:
                        dict_to_add_files_to[name] = f.read()

    return document
